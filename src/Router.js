import React from "react";
import {
    BrowserRouter,
    Switch,
    Route,
    useHistory
} from "react-router-dom";
import Home from "./components/Home";
import Single from "./components/Single";
import Archive from "./components/Archive";
import Page from "./components/Page";
import DocumentMeta from "./components/DocumentMeta";
import Authen from "./api/jwt-auth";

export default class Router extends React.Component {

    History = () => {
        let history = useHistory();
        history.push("/search");
    }
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" >
                        <DocumentMeta />
                        <Home />
                    </Route>
                    <Route exact path="/test">
                    </Route>
                    <Route path="/auth" >
                        <Authen />
                    </Route>
                    <Route exact path="/search" render={(props) => <Archive param={props} type="search" />} />
                    <Route path="/privacy" >
                        <DocumentMeta title="PRIVACY POLICY" />
                        <Page name="privacy-policy" />
                    </Route>

                    <Route exact path="/product" render={() => <div><DocumentMeta title="PRODUCT" /><Archive type="product" /></div>} />
                    <Route path="/product/:id" render={(props) => <Single param={props} type="product" />} />

                </Switch>
            </BrowserRouter >
        );
    }
}
