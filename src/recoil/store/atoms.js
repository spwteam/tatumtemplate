import { atom } from 'recoil';
export const personState = atom({
    key: 'personState',
    default: {
        user: '1234',
        pass: '5678'
    }
});

export const searchState = atom({
    key: 'searchState',
    default: {
        search: ''
    }
});
