import { selector } from 'recoil';
import { searchState } from './atoms';
export const lenSearchState = selector({
    key: 'lenSearchState',
    get: ({ get }) => {
        const searchObj = get(searchState);
        return searchObj.search.length;
    }
});
