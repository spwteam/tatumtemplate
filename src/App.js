import React, { PureComponent } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import Router from './Router'
import ReactGA from 'react-ga'
import { RecoilRoot } from 'recoil';

ReactGA.initialize(process.env.REACT_APP_GA);


export default class App extends PureComponent {

  render() {
    return (
      <RecoilRoot>
        <Router />
      </RecoilRoot>
    );
  }
}