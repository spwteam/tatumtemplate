import jsonPlaceholder from '../../api/jsonPlaceholder'

export const fetchPosts = (type, search = "", page = 1) => async dispatch => {
  let url = "";
  switch (type) {
    case "search":
      url = "/search?_embed&subtype=shop&per_page=12&page=" + page + "&search=" + search;
      break;
    case "shop":
      url = "/" + this.state.type + "?_embed&per_page=8";
      break;
    case "post":
      url = "/" + this.state.type + "?_embed&per_page=8";
      break;
    default:
      url = "/shop?_embed&per_page=12";
      break;
  }
  const response = await jsonPlaceholder.get(url)
  // const response = await jsonPlaceholder.get('/posts')
  dispatch({ type: 'FETCH_POSTS', payload: response.data })
}

export const searchValue = content => ({ type: 'SEARCH_VALUE', payload: {content} })

