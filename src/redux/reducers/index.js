import { combineReducers } from 'redux'
import postsReducer from '../reducers/postReducer'
import searchReducer from '../reducers/searchValue'

export default combineReducers({
    posts: postsReducer,
    search: searchReducer
})

