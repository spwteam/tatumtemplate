export const Menus = {
    data: [{
        "to": "/",
        "className": "nav-link",
        "activeClassName": "active",
        "name": "HOME",
        "target": ""
    }, {
        "to": "/product",
        "className": "nav-link",
        "activeClassName": "active",
        "name": "PRODUCT",
        "target": ""
    }, {
        "to": "/privacy",
        "className": "nav-link",
        "activeClassName": "active",
        "name": "PRIVACY POLICY",
        "target": ""
    }]
};