import React from 'react';
import { render } from '@testing-library/react';
import "bootstrap/dist/css/bootstrap.min.css";
import 'bootstrap';
import Router from './Router';
import ReactGA from 'react-ga';


ReactGA.initialize('dummy', { testMode: true });

test('Check HOME PAGE', () => {
  const { getByText } = render(<Router />);
  const linkElement = getByText(/HOME/i);
  expect(linkElement).toBeInTheDocument();
});
