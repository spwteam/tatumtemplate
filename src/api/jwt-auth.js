import React, { Component } from 'react';
import axios from 'axios';

export default class Authen extends Component {
    constructor(prop) {
        super(prop);
        this.state = {
            username: process.env.REACT_APP_CLIENT_ID,
            password: process.env.REACT_APP_CLIENT_SECRET,
            message: '',
        };
    }

    static async getInitialProps() {
        return '';
    }

    login() {
        let message = '';
        this.setState({
            message: ''
        });
        const { username, password } = this.state;
        axios
            .post(`${process.env.REACT_APP_APIURL}/jwt-auth/v1/token`, {
                username,
                password,
            })
            .then(res => {
                const { data } = res;
                localStorage.setItem(process.env.REACT_APP_TOKEN, data.token);
                localStorage.setItem(process.env.REACT_APP_USERNAME, data.user_nicename);
                //Router.push('/');
            })
            .catch(() => {
                message =
                    'Sorry, that username and password combination is not valid.';
                this.setState({ message });
            });
    }

    UNSAFE_componentWillMount() {
        let message = '';
        const { username, password } = this.state;
        axios.post(`${process.env.REACT_APP_CLIENT_TOKEN_URL}`, {
            grant_type : "client_credentials"
        }, {
            auth: {
                username,
                password
            }
        })
            .then(res => {
                const { data } = res;
                localStorage.setItem("token", data.access_token);
                //localStorage.setItem(process.env.REACT_APP_USERNAME, data.user_nicename);
                //Router.push('/');
            })
            .catch(() => {
                message =
                    'Sorry, that username and password combination is not valid.';
                this.setState({ message });
            });
    }

    render() {
        const { message } = this.state;
        return (
            <div className="">
                <p>{message}</p>
                <p>{localStorage.getItem("token")}</p>
            </div>
        )

    }
}