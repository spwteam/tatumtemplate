import axois from 'axios'
 
export default axois.create({
    baseURL: 'http://shopper.yoskai.com/wp-json/wp/v2'
})