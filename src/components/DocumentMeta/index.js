import React from 'react';
import MetaTags from 'react-document-meta';
import ReactGA from 'react-ga';


export default class DocumentMeta extends React.Component {
    state = {
        title: (this.props.title ? this.props.title + " - " + process.env.REACT_APP_TITLE : process.env.REACT_APP_TITLE),
        desc: (this.props.desc ? this.props.desc : process.env.REACT_APP_DESC),
        image: (this.props.image ? this.props.image : process.env.REACT_APP_BASE_URL + "/" + process.env.REACT_APP_IMAGE)
    }
    componentWillReceiveProps(nextProps) {

        if (nextProps.title !== this.props.title) {
            this.setState({
                title: nextProps.title
            });
        }

        if (nextProps.desc !== this.props.desc) {
            this.setState({
                desc: nextProps.desc
            });
        }

        if (nextProps.image !== this.props.image) {
            this.setState({
                image: nextProps.image
            });
        }
    }

    render() {
        const hostname = window.location.hostname;
        const url = window.location.href;
        ReactGA.pageview(this.state.title);
        return (
            <div>
                <MetaTags>
                    <title>{this.state.title}</title>
                    <meta id="meta-description" name="description" content={this.state.desc} />
                    <meta id="og-title" property="og:title" content={this.state.title} />
                    <meta id="og-description" property="og:description" content={this.state.desc} />
                    <meta id="og-url" property="og:url" content={url} />
                    <meta id="og-type" property="og:type" content="website" />
                    <meta id="og-image" property="og:image" content={this.state.image} />
                    <meta name="twitter:card" content={this.state.title + " " + this.state.desc + " " + url}></meta>
                    <meta name="robots" content="noindex, nofollow"></meta>
                    <link rel="canonical" href={hostname}></link>
                </MetaTags>
                <div className="content d-none">{this.state.title + " " + this.state.desc + " " + url}</div>
            </div>
        )
    }
}