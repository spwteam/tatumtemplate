import React from "react";
import Header from "../Header";
import Footer from "../Footer";
import Article from "../common/article"
import queryString from "query-string";

export default class Archive extends React.Component {
    constructor(props) {
        super(props);
        if (props.param) {
            let param = queryString.parse(this.props.param.location.search);
            this.search = param.search;
        } else {
            this.search = null;
        }
        this.type = (props.type ? props.type : null);
    }
    render() {
        return (
            <div>
                <Header />
                <main>
                    {this.Main(this.type)}
                </main>
                <Footer />
            </div>
        )
    }

    Main = (type) => (
        <div className="container">
            <section className="news">
                <Article type={type} search={this.search} />
            </section>
        </div>
    )
}