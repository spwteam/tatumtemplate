import React from "react";

export default class Carousel extends React.Component {

    state = {
        error: null,
        isLoaded: false,
        items: []
    };

    componentDidMount() {
        fetch("data/carousel.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="row">
                    <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators">
                            {items.map((item, index) => (
                                <li data-target="#carouselExampleIndicators" key={index} data-slide-to={index} className={(index === 0) ? 'active' : ''}></li>
                            ))
                            }
                        </ol>
                        <div className="carousel-inner">
                            {items.map((item, index) => (
                                <div key={index} className={(index === 0) ? 'active carousel-item' : 'carousel-item'}>
                                    <img src={item.image} className="d-block w-100" alt={item.name} />
                                </div>
                            ))
                            }
                        </div>
                        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div >
            );
        }
    }

}