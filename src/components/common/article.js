import React from 'react';
import {
    Link
} from 'react-router-dom';

class Article extends React.Component {
    state = {
        error: null,
        isLoaded: false,
        items: [],
        type: (this.props.type ? this.props.type : null),
        search: (this.props.search ? this.props.search : ''),
        page: (this.props.page ? this.props.page : 1),
        per_page: (this.props.per_page ? this.props.per_page : 8)
    };

    componentDidMount() {
        let url = ""
        switch (this.state.type) {
            case "search":
                url = "/search?_embed&subtype=shop&per_page=12&page=" + this.state.page + "&search=" + this.state.search;
                break;
            case "shop":
                url = "/" + this.state.type + "?_embed&per_page=8";
                break;
            case "post":
                url = "/" + this.state.type + "?_embed&per_page=8";
                break;
            default:
                url = "/shop?_embed&per_page=12";
                break;
        }
        fetch('http://shopper.yoskai.com/wp-json/wp/v2' + url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }


    renderList = (items) => {
        return items.map((item, index) => {
            return (
                <article key={index} className="col-12 col-sm-6 col-md-4 col-lg-3 border border-dark py-3">
                    <div><Link to={{ pathname: "/product/" + item.id }}><img src={item.fimg_url} alt={item.title.rendered} className="img-thumbnail" /></Link></div>
                    <h5 className="text-center pt-2">{item.title.rendered}</h5>
                </article>
            )

        })
    }

    rederSearchList = (items) => {
        return items.map((item, index) => {
            return (
                <article key={index} className="col-12 col-sm-6 col-md-4 col-lg-3 border border-dark py-3">
                    <div><Link to={{ pathname: "/product/" + item.id }}><img src={item._embedded.self[0].fimg_url} alt={item.title} className="img-thumbnail" /></Link></div>
                    <h5 className="text-center pt-2">{item.title}</h5>
                </article>
            )
        })
    }

    typeRender = (items) => {
        return items.map((item, index) => {
            return (
                <article key={index} className="col-12 col-sm-6 col-md-4 col-lg-3 border border-dark py-3">
                    <div><Link to={{ pathname: "/product/" + item.id }}><img src={item._embedded.self[0].fimg_url} alt={item.title} className="img-thumbnail" /></Link></div>
                    <h5 className="text-center pt-2">{item.title}</h5>
                </article>
            )
        })
    }


    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            if (this.state.type === "search") {
                if (items.length === 0) {
                    return (
                        <div className="row">
                            <h4 className="p-4 text-center">Keyword : {this.state.search}</h4>
                            <p className="p-4 text-center">Item not found<br />Please try to search again</p>
                        </div>
                    )
                } else {
                    return (
                        <div className="row">
                            {this.rederSearchList(items)}
                        </div>
                    );
                }
            } else {
                return (
                    <div className="row">
                        {this.renderList(items)}
                    </div>
                );
            }
        }
    }

}

export default Article;