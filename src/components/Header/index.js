import React from 'react'
import {
    Link,
    NavLink,
    useHistory
} from 'react-router-dom';
import { Menus } from '../../data/menu'
import { useRecoilState } from 'recoil'
import { searchState } from '../../recoil/store'
import queryString from "query-string";

const Header = () => {
    let history = useHistory();
    const [searchObj, setSearchObj] = useRecoilState(searchState)
    const logoImage = () => (
        <img src="https://www.viz-card.com/images/logo.png" alt="logo" className="main_logo"></img>
    )

    const logo = () => (
        <div className="logo col-12 col-lg-auto px-0 d-flex d-lg-block justify-content-between">
            <Link to="/" className="navbar-brand">
                {logoImage()}
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
        </div>
    )

    const menu = (search) => (
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto mb-2 mb-lg-0">
                {Menus.data.map((item, index) =>
                    <li key={index} className="nav-item">
                        <NavLink to={item.to} className={item.className} exact={true}>{item.name}</NavLink>
                    </li>
                )}
            </ul>
            {searchItem(search)}
        </div>
    )

    const searchItem = (search) => (
        <form className="d-flex" onSubmit={handleSubmit}>
            <input className="form-control mr-2" type="text" name="search" placeholder="Search" value={search} onChange={handleChange} />
            <button className="btn btn-outline-success" type="submit" >Search</button>
        </form>
    )

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(history)
        history.push('/search?prev=' + history.location.pathname + '&search=' + event.target.search.value)
        // history.replace('/search');
        let param = queryString.parse(history.location.search);
        if (param.prev === "/search") {
            history.go(0)
        }
    }

    const handleChange = (event) => {
        setSearchObj({ search: event.target.value })
    }

    return (
        <header className="header mb-3">
            <div className="container">
                <div className="row">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <div className="container-fluid ">

                            {logo()}

                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                {menu(searchObj.search)}
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
    )
}

export default Header