import React from 'react';
import { connect } from "react-redux";
import { toggleTodo } from "../../redux/actionTypes";

const searchItem = (state) => (
    <form className="d-flex" method="get" action="/search">
        <input className="form-control mr-2" type="search" name="search" placeholder="Search" aria-label="Search" />
        <button className="btn btn-outline-success" type="submit">Search</button>
    </form>
)


// export default Todo;
export default connect(null, { searchItem });