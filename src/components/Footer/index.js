import React from "react";


export default class Footer extends React.Component {

    render() {
        return (
            <footer className="footer py-3">
                <div className="container">
                    {this.copyright(2020)}
                </div>
            </footer>
        )
    }

    copyright = (year = "2020") => (
        <p className="text-center">
            copyright {year}
        </p>
    )



}