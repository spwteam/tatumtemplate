import React from "react"
import {
    Link,
    NavLink,
    useHistory
} from 'react-router-dom';
import Header from "../Header"
import Footer from "../Footer"
import $ from 'jquery'
import { userIDState, userProfileState } from '../../recoil/store'
import queryString from "query-string";

let sessvars = { language: "th" }
var lang = {
    "th": [
        "<h2><span class='pink'>ส</span>มาชิก</h2><h2><img src='images/viz.png' class='viz-word'/> CARD </h2>",
        "สมัครสมาชิก <img src='images/viz.png' class='viz-word'/> CARD ผ่านแอปพลิเคชัน ONESIAM เพื่อเริ่มต้นสิทธิ์พิเศษเหนือระดับกับ <img src='images/viz.png' class='viz-word'/> WHITE CARD ",
        "<img src='images/viz.png' class='viz-word'/> CARD มอบความพิเศษเหนือระดับสำหรับคนพิเศษเช่นคุณ<br/><br/>ส่วนลดจากกว่า 600 ร้านค้า สูงสุด 20% สยามเซ็นเตอร์ สยามดิสคัฟเวอรี่ สยามพารากอน และไอคอนสยาม และพิเศษยิ่งขึ้นเมื่อช้อปครบ 20,000 บาท ภายในหนึ่งวันรับสิทธิ์อัพเกรดเป็น <img src='images/viz.png' class='viz-word'/>  Titanium Card เพื่อคนพิเศษเช่นคุณ",
        "สิทธิประโยชน์​สมาชิก  <img src='images/viz.png' class='viz-word'/>  WHITE CARD",
        "<img src='images/b1-2-th.jpg' class='img-fluid'/>",
        "หมายเหตุ: การสมัครสมาชิก  <img src='images/viz.png' class='viz-word'/>  WHITE CARD ผ่านแอพพลิเคชัน ONESIAM เปิดให้บริการสำหรับคนไทยเท่านั้น  (ต้องใช้หมายเลขบัตรประชาชนในการสมัคร) <br/><br/>หากท่านประสงค์จะสมัครด้วย Passport ID <br/><a href='https://touristregistration.onesiam.com/' class='pink pb-5 mb-5'>กรุณาสมัคร Tourist Card</a>",
        "APPLY NOW"
    ],
    "en": [
        "<h2><img src='images/viz.png' class='viz-word'/> CARD </h2><h2><span class='pink'>M</span>EMBER</h2>",
        "Apply for  <img src='images/viz.png' class='viz-word'/> CARD Membership through ONESIAM Application to Begin Your Extraordinary Privileges with <img src='images/viz.png' class='viz-word'/> WHITE CARD ",
        "<img src='images/viz.png' class='viz-word'/> CARD offers extraordinary privileges for exceptional person like you.<br/><br/>Discounts from over 600 stores, up to 20%, at Siam Center, Siam Discovery, Siam Paragon, and ICONSIAM. <br/>Plus, for purchasing from 20,000 baht within a day, you will be upgraded to <img src='images/viz.png' class='viz-word'/> Titanium Card. ",
        "Privileges for   <img src='images/viz.png' class='viz-word'/> WHITE CARD Members",
        "<img src='images/b1-2-en.jpg' class='img-fluid'/>",
        "Note: Applying for <img src='images/viz.png' class='viz-word'/>  WHITE CARD WHITE CARD through OneSiam Application is available only for Thai citizens (the national ID card number is required).<br/><br/>If you would like to apply with your passport ID number,<br/><a href='https://touristregistration.onesiam.com/' class='pink pb-5 mb-5'>please apply for Tourist Card.</a>",
        "APPLY NOW"
    ],
    "ch": [
        "<h2><img src='images/viz.png' class='viz-word'/> CARD </h2><h2 style='line-height:40px;font-size:32px;'><span class='pink'>会</span>员</h2>",
        "通过 ONESIAM 应用软件注册  <img src='images/viz.png' class='viz-word'/> CARD 会员可获取 <img src='images/viz.png' class='viz-word'/> WHITE CARD 会员特别优惠 ",
        "<img src='images/viz.png' class='viz-word'/> CARD 为特别的您提供专属优惠<br/><br/>OneSiam商圈暹罗中心、暹罗探索、暹罗百丽宫和暹罗天地共超过600家商户最高八折优惠 <br/>一日内消费超过20,000泰铢更将获得会员升级成为 <img src='images/viz.png' class='viz-word'/>  Titanium Card 会员 <br/>为特别的您提供专属优惠",
        "<img src='images/viz.png' class='viz-word'/>  WHITE CARD 会员专属权益",
        "<img src='images/b1-2-ch.jpg' class='img-fluid'/>",
        "注意： 通过 OneSiam 应用软件注册   <img src='images/viz.png' class='viz-word'/>  CARD 会员活动只针对泰国公民（需要提供泰国身份证号）<br/><br/>如果您需要申请国际旅客优惠卡<br/><a href='https://touristregistration.onesiam.com/' class='pink pb-5 mb-5'>请申请注册 Tourist Card</a>",
        "APPLY NOW"
    ]

}

var current_lang = "";
$("button#onesiam_detail").click(function () {
    var language = current_lang;
    sessvars.language = current_lang;
    window.location = 'onesiam_white.php?language=' + language;
});
$("button#how_to_login").click(function () {
    var language = current_lang;
    sessvars.language = current_lang;
    window.location = 'how_to_login.php?language=' + language;
});

function back() {
    window.history.back();
    return false;
}
function backHome() {
    window.location = 'https://vizregister.onesiam.com/backHome';
    return false;
}
function hideLoading() {
    $('#modalWait').on('shown.bs.modal', function (e) {
        $("#modalWait").modal('hide');
    })
}

if (sessvars.language === "undefined" || sessvars.language === null) {
    if (current_lang === "") {
        changeLanguage("th");
    } else if (current_lang === "ch") {
        changeLanguage("中文");
    } else {
        changeLanguage("");
    }
} else {
    var var_language = sessvars.language;
    if (var_language === "ch") {
        changeLanguage("中文");
    } else {
        changeLanguage(var_language.substring(0, 2));
    }
}
$("button#th-language").click(function () {
    changeLanguage("th");
});
$("button#en-language").click(function () {
    changeLanguage("en");
});
$("button#ch-language").click(function () {
    changeLanguage("中文");
});
function changeLanguage(language) {
    sessvars.language = language;
    $("body").removeClass("th");
    $("body").removeClass("en");
    $("body").removeClass("ch");
    if (language === "th") {
        current_lang = language;
        $("body").addClass(language);
        $("#current-language").html(language + " <i class='fas fa-chevron-down'></i>");
        $("span#lang").each(function (index) {
            $(this).html(lang.th[index]);
        });
    } else if (language === "en") {
        current_lang = language;
        $("body").addClass(language);
        $("#current-language").html(language + " <i class='fas fa-chevron-down'></i>");
        $("span#lang").each(function (index) {
            $(this).html(lang.en[index]);
        });
    } else {
        current_lang = "ch";
        $("body").addClass("ch");
        $("#current-language").html(language + "<i class='fas fa-chevron-down'></i>");
        $("span#lang").each(function (index) {
            $(this).html(lang.ch[index]);
        });
    }
}

const Home = () => {
    state = {
        error: null,
        isLoaded: false,
        items: []
    }
    // this.handleClick = this.handleClick.bind(this)



    const handleClick = (event) => {
        console.log("click")
    }

    render = () => {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return (
                <div>
                    <main>Loading...</main>
                </div>
            )
        } else {
            console.log(items)
            return (
                <div>
                    <Header />
                    <main>
                        {this.Main(items)}
                    </main>
                    <Footer />
                </div>
            )
        }
    }


    Main = () => (
        <div id="main" className="position-relative overflow-hidden" >
            <div className="position-absolute text-right" style={{ right: "-130px", zIndex: "-1", marginTop: "60px" }}><img src="images/b1-1.png" alt="hold mobile" style={{ minWidth: "320px", width: "100%", maxWidth: "515px" }} /></div>

            <div className="container" style={{ paddingBottom: "80px" }}>
                <div className="col-12">
                    <h6 className="mb-4">
                        <span id="lang"></span>
                    </h6>

                    <div style={{ width: "100%", height: "400px" }}></div>


                    <p className="mb-4">
                        <span id="lang"></span>
                    </p>
                    <h6 className="mb-4 hint"><span id="lang"></span></h6>

                    <div className="text-center mb-4"><span id="lang"></span></div>

                    <p className="pb-5 mb-5">
                        <span id="lang"></span>
                    </p>

                    <input type="hidden" name="customerID" id="customerID" value="" />
                    <input type="hidden" name="email" id="email" value="" />
                    <div className="fixed-bottom">
                        <div className="transparent"></div>
                        <div className="text-center py-5">
                            <button id="next" type="button" onClick={this.handleClick} className="position-relative col-8 col-sm-4 py-2 pink font-weigt-bold colorWhite backgroundPink">
                                <span id="lang"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

    return (
        <div>

        </div>
    )
}

export default Home