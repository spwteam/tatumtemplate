import React from 'react'
import Header from '../Header'
import Footer from '../Footer'
import DocumentMeta from '../DocumentMeta'

class Single extends React.Component {
    state = {
        error: null,
        isLoaded: false,
        items: {}
    };


    componentDidMount() {

        fetch('http://shopper.yoskai.com/wp-json/wp/v1/shop/' + this.props.param.match.params.id)
            .then(res => {
                if (res.ok) return res.json();
            }
            )
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    render() {
        const { error, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else {
            return (
                <div>
                    <DocumentMeta title={items.name_en} image={items.image} desc={items.name_en + " " + items.name_th + " " + items.location + " " + items.floor} />
                    <Header />
                    <main>
                        <div className="container">
                            <article className="py-3 px-4">
                                <div className="col-12 col-sm-4 mx-auto text-center mb-2"><img src={items.image} alt={items.name_en} className="img-thumbnail" /></div>
                                <h4 className="text-center">{items.name_en}</h4>
                                <h4 className="text-center">{items.name_th}</h4>
                                <div className="content mt-3 p-3 border border-primary">
                                    <p>Location : {items.location}</p>
                                    <p>Floor : {items.floor}</p>
                                </div>
                            </article>
                        </div>
                    </main>
                    <Footer />
                </div>
            )
        }
    }
}

export default Single