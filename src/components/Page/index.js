import React from "react";
import Header from "../Header";
import Footer from "../Footer";

export default class Page extends React.Component {
    state = {
        error: null,
        isLoaded: false,
        items: {}
    };

    componentDidMount() {
        fetch("http://shopper.yoskai.com//wp-json/wp/v2/pages/?slug=" + this.props.name)
            .then(res => {
                if (res.ok) return res.json();
            }
            )
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result[0]
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div><Header /><div className="container">Loading...</div><Footer /></div>;
        } else {
            return (
                <div>
                    <Header />
                    <main>
                        <div className="container">
                            <article className="py-3 px-4">
                                <h4 className="text-center mb-4">{items.title.rendered}</h4>
                                <div dangerouslySetInnerHTML={{ __html: items.content.rendered }} />
                            </article>
                        </div>
                    </main>
                    <Footer />
                </div>
            )
        }
    }
}